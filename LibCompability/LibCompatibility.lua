LibCompatibility={}

-- function LibCompatibility.GetPowerType(t, key)
--     DEFAULT_CHAT_FRAME:AddMessage(key)
--     return key
-- end

function LibCompatibility:OnLoad()
end

function LibCompatibility:OnEvent(event)
end

function LibCompatibility:OnUpdate()
end

Enum = {
    PowerType = {
        Mana          = 0,
        Rage          = 1,
        Focus         = 2,
        Energy        = 3,
        ComboPoints   = 4,
        Runes         = 5,
        RunicPower    = 6,
        SoulShards    = 7,
        LunarPower    = 8,
        HolyPower     = 9,
        Alternate     = 10,
        Maelstrom     = 11,
        Chi           = 12,
        Insanity      = 13,
        Obsolete      = 14,
        Obsolete2     = 15,
        ArcaneCharges = 16,
        Fury          = 17,
        Pain          = 18,
        NumPowerTypes = 19,
    }
}

function LibCompatibility:is_equal(a, b, epsilon)
    if not epsilon then
        epsilon = 1e-6
    end
    return math.abs(a-b) < epsilon and true or false
end

function LibCompatibility:CheckAllSpellsAndSelectSeveral(gcd_time, min_gcd_spells_to_select, selecting_num)
    local spells_with_gcd_duration = {}
    local selecting_spells = {}
    for i = 1, MAX_SKILLLINE_TABS do
        local _, _, offset, numSpells = GetSpellTabInfo(i);
        for spell_id = offset + 1, offset + numSpells do
            local start, duration, enabled = GetSpellCooldown(spell_id, BOOKTYPE_SPELL)
            if self:is_equal(gcd_time, duration) then
                table.insert(spells_with_gcd_duration, spell_id)
            end
        end
    end

    if getn(spells_with_gcd_duration) >= min_gcd_spells_to_select then
        for gcd_spell_index=1, math.min(getn(spells_with_gcd_duration), selecting_num) do
            table.insert(selecting_spells, spells_with_gcd_duration[gcd_spell_index])
        end
    end

    return selecting_spells
end

local selected_spells_for_check = {}
function LibCompatibility:IsGlobalCooldown()
    if getn(selected_spells_for_check) == 0 then
        local selected_spells = self:CheckAllSpellsAndSelectSeveral(1.5, 5, 3)
        if getn(selected_spells) > 0 then
            selected_spells_for_check = selected_spells
            return true
        end
    else
        local is_cooldown = true
        for _, spell_id in ipairs(selected_spells_for_check) do
            local start, duration, enabled = GetSpellCooldown(spell_id, BOOKTYPE_SPELL)
            if not self:is_equal(duration, 1.5) then
                is_cooldown = false
                break
            end
        end
        return is_cooldown
    end
    return false
end

-- local mt = {__index = LibCompatibility.GetPowerType}
-- setmetatable(Enum.PowerType, mt)
