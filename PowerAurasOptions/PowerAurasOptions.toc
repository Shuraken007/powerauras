## Interface: 11305
## Title: Power Auras Classic Options
## Version: 4.32.2
## Author: Smacker 'lolcat', Dridzt 'classic', Garstiger 'continued', Sinsthar 'base', Meorawr, modified by Zensunim and Resike
## Notes: Options and configuration GUI for Power Auras Classic.
## DefaultState: Enabled
## Dependencies: PowerAuras

Libs\LibStub\LibStub.lua
Libs\MSA-DropDownMenu-1.0\MSA-DropDownMenu-1.0.lua

PowerOptions.lua
PowerOptionsData.lua
PowerOptionsDump.lua
PowerComms.lua

PowerOptions.xml
PowerOptionsBlizzard.xml
